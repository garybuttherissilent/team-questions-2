import subprocess
from pathlib import Path

import yaml
from requests.auth import HTTPBasicAuth


def _get_password():
    current_folder = Path(__file__).parent

    config_folder = current_folder.parent.parent.joinpath("teamquestions", "backend", "utils", "management", "config")
    vindevoy_pwd_yaml_file_name = config_folder.joinpath("vindevoy_pwd.secret")

    with open(vindevoy_pwd_yaml_file_name, 'r') as file:
        vindevoy_pwd_yaml = yaml.load(file, Loader=yaml.FullLoader)

    return vindevoy_pwd_yaml["password"]


def get_authentication():
    return HTTPBasicAuth('vindevoy', _get_password())
