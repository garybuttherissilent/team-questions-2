import requests

from common import get_authentication

_QUESTIONS_URL = "http://127.0.0.1:8000/questions/"
_NEW_ID = 3

def test_list():
    response = requests.get(url=_QUESTIONS_URL)

    assert response.status_code == 200
    data = response.json()

    assert type(data) is dict

    assert "count" in data
    assert data["count"] == 1

    assert "next" in data
    assert "previous" in data
    assert "results" in data

    records = data["results"]

    assert type(records) is list
    assert len(records) == data["count"]

    question = records[0]
    assert question["id"] == 1
    assert question["title"] == "First question"


def test_new():
    global _NEW_ID

    new_question = {
        "title": "Test question",
        "text": "This is a test question",
        "author": "http://127.0.0.1:8000/users/2/"
    }

    response = requests.post(_QUESTIONS_URL, new_question, auth=get_authentication())
    data = response.json()

    _NEW_ID = data["id"]


def test_delete():
    global _NEW_ID

    response = requests.delete(f"{_QUESTIONS_URL}{_NEW_ID}/", auth=get_authentication())

    assert response.status_code == 204


if __name__ == "__main__":
    test_list()
    test_new()
    test_delete()
