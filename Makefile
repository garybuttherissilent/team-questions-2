drop_db:
	@python ./teamquestions/backend/utils/database/drop_db.py

drop-db: drop_db

create_db:
	@python ./teamquestions/backend/utils/database/create_db.py

create-db: create_db

init_db:
	python ./teamquestions/backend/manage.py makemigrations
	python ./teamquestions/backend/manage.py migrate
	python ./teamquestions/backend/manage.py auto_createsuperuser


init-db: init_db

sample_db:
	python ./teamquestions/backend/manage.py loaddata ./teamquestions/backend/utils/sample_data/001_groups.yaml
	python ./teamquestions/backend/manage.py loaddata ./teamquestions/backend/utils/sample_data/002_users.yaml
	python ./teamquestions/backend/manage.py loaddata ./teamquestions/backend/utils/sample_data/003_questions.yaml
	python ./teamquestions/backend/manage.py update_users

sample-db: sample_db

reset_db: drop_db create_db init_db sample_db

reset-db: reset_db

run_backend: init_db
	poetry run ./teamquestions/backend/manage.py runserver 8000

run-backend: run_backend

run_frontend:
	poetry run ./teamquestions/frontend/main.py
