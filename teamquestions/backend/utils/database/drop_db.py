from sqlalchemy import create_engine
from sqlalchemy import text


def drop_db():
    engine = create_engine("postgresql+psycopg2://postgres:postgres@localhost/postgres", isolation_level="AUTOCOMMIT", echo=False)

    sql_drop_db = text("DROP DATABASE IF EXISTS teamquestions;"), "Dropping database if exists"
    sql_drop_role = text("DROP ROLE IF EXISTS teamquestions;"), "Dropping role if exists"

    sql_commands = [sql_drop_db, sql_drop_role]

    try:
        with engine.connect() as connection:
            for sql in sql_commands:
                print(sql[1])
                connection.execute(sql[0])

    except Exception as err:
        print(f"Cannot drop the database (most likely, there's an open connection to it):\n\n{err}")


if __name__ == "__main__":
    drop_db()
