import flet as ft

from common.globals import APPLICATION_NAME, APPLICATION_VERSION


def main(page: ft.Page):
    page.title = APPLICATION_NAME
    page.vertical_alignment = ft.MainAxisAlignment.START

    txt_welcome = ft.Text(value=f"{APPLICATION_NAME} - version {APPLICATION_VERSION}", text_align=ft.TextAlign.CENTER)

    page.add(txt_welcome)


ft.app(target=main, view=ft.AppView.WEB_BROWSER, port=8001)
